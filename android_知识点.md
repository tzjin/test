---
android
---

https://juejin.im/entry/59dd75cd51882578d5037626
<br />
https://github.com/francistao/LearningNotes
# 源码
https://mirrors.tuna.tsinghua.edu.cn/help/AOSP/
<br />
https://source.android.com 
<br />
https://www.googlesource.com/
<br />
https://storage.googleapis.com/
# 进程
     1. 前台进程：
    即与用户正在交互的Activity或者Activity用到的Service等，如果系统内存不足时前台进程是最后被杀死的
    
     2. 可见进程：
    可以是处于暂停状态(onPause)的Activity或者绑定在其上的Service，即被用户可见，但由于失去了焦点而不能与用户交互.
    
     3. 服务进程：
    其中运行着使用startService方法启动的Service，虽然不被用户可见，但是却是用户关心的，例如用户正在非音乐界面听的音乐或者正在非下载页面自己下载的文件等；当系统需要空间运行前两者进程(–>指的是前台进程和可见进程)时才会被终止.
    
     4. 后台进程：
    其中运行着执行onStop方法而停止的程序，但是却不是用户当前关心的，例如后台挂着的QQ，这样的进程系统一旦没了有内存就首先被杀死.
    
     5. 空进程：
    不包含任何应用程序的程序组件的进程，这样的进程系统是一般不会让他存在的，为了进行缓存，使下次App启动的时候更快，当系统需要内存是最先被杀死.

# 线程

## 并发集合
    vector：就比arraylist多了个线程安全
    hashtable：就比hashmap多了个线程安全
    StringBuffer 线程安全
 ## 线程变量
    ThreadLocal: 提供线程内的局部变量 (Looper里用到)
    Thread里有ThreadLocal.Values localValues= 类似ThreadLocalMap
    ThreadLocal通过currentThread  get,set数据
    
    ThreadLocalMap：是使用ThreadLocal的弱引用作为Key的
## 锁
https://www.cnblogs.com/fanguangdexiaoyuer/p/5313653.html
<br />http://blog.csdn.net/u011760575/article/details/53999596

    Volatile(内存和CPU高速缓存,针对变量):
    Jvm 将硬件内存抽象成jvm内存管理(比较慢)， 
    volatile是指定了 jvm内存与工作空间内存的同步情况
    Synchronized(JVM) jvm可自动释放
    ReentrantLock(Lock CPU总线，代码实现的)：
    是一种递归无阻塞的同步机制,可中断等待干其他的
    比Synchronized多了 锁投票，定时锁等候和中断锁等候
    ReentrantLock.tryLock 检查是否被其他线程占用
    Atomic: 一段代码中只能出现一个Atomic的变量

在资源竞争不是很激烈的情况下，Synchronized的性能要优于ReetrantLock，
但是在资源竞争很激烈的情况下，Synchronized的性能会下降几十倍，尔ReetrantLock的性能能维持常态
# 流程
## 点击 Android Studio 的 build 按钮后发生了什么（android的编译过程）
http://blog.csdn.net/zheng548/article/details/54864765

    1. Preparation of dependecies 在这个阶段gradle检测module依赖的所有library是否ready。如果这个module依赖于另一个module，则另一个module也要被编译。 
    2. Merging resources and processing Manifest 在这个阶段之后，资源和Manifest文件被打包。 
    3. Compiling 在这个阶段处理编译器的注解，源码被编译成字节码。 
    4. Postprocessing 所有带 “transform”前缀的task都是这个阶段进行处理的。 
    5. Packaging and publishing 这个阶段library生成.aar文件，application生成.apk文件。

![enter description here][1]

    1. 使用Android Asset Packaging Tool(aapt) ，将AndroidManifest.xml和res下的资源编译生成R.java文件，这样java文件就可以去引用资源了 - 
    2. 使用aidl工具去生成对应的Java interfaces - 
    3. 将src和通过aapt生成的R.java，.aidl文件通过javaC命令去生成.class 文件 - 
    4. 使用dex tool 将class文件转化成Dalvik byte code.这时候要将所有class文件和第三方的jar包都包括。 - 
    5. 所有没有编译过得图片和编译过的图片,.dex文件传给apkbuilder去打包成.apk - 
    6. 最后采用zipalign tool 打入签名

## 一个应用程序安装到手机上时发生了什么
https://www.jianshu.com/p/953475cea991
<br />
检查权限

    1. 复制apk到data/app目录
    2. 解析apk信息
        * 设置系统App的一些参数
        * 校验签名
        * 解析app的provider,校验是否与已有的provider冲突
        * 32/64位abi的一些设置
        * 四大组件的解析，注册
    3. dexopt操作
        * Dalvik：优化dex放到data/dalvik-cache
        * Art:：翻译dex得到oat
    4. 更新权限信息
    5. 完成安装,发送Intent.ACTION_PACKAGE_ADDED广播
    
![enter description here][2]

## Android系统默认Home（Launcher）的启动过程小结
http://blog.csdn.net/happy08god/article/details/24265167
## App启动流程，从点击桌面开始
https://www.jianshu.com/p/a72c5ccbd150
<br />
https://www.jianshu.com/p/a5532ecc8377
![enter description here][3]

    ① 点击桌面App图标，Launcher进程采用Binder IPC向system_server进程发起startActivity请求；
    ② system_server进程接收到请求后，向zygote进程发送创建进程的请求；
    ③ Zygote进程fork出新的子进程，即App进程；
    ④ App进程，通过Binder IPC向sytem_server进程发起attachApplication请求；
    ⑤ system_server进程在收到请求后，进行一系列准备工作后，再通过binder IPC向App进程发送scheduleLaunchActivity请求；
    ⑥ App进程的binder线程（ApplicationThread）在收到请求后，通过handler向主线程发送LAUNCH_ACTIVITY消息；
    ⑦ 主线程在收到Message后，通过发射机制创建目标Activity，并回调Activity.onCreate()等方法。
    ⑧ 到此，App便正式启动，开始进入Activity生命周期，执行完onCreate/onStart/onResume方法，UI渲染结束后便可以看到App的主界面

## Activity启动(实现未注册的启动)
https://github.com/Jerey-Jobs/AppPluginDemos 

    ContextImpl.startActivity
    Instrumentation.execStartActivity
    ActivityManagerNative.ActivityManagerProxy.startActivity //入口，在这里用代理,
    aidl 调用到ActivityManagerService.startActivity

1. 代理拦截ActivityManagerNatived的startActivity 
替换intent,欺骗ActivityManagerService的一些检查,
从而可以顺利回调到ActivityThread.handler中,
2. 真正启动Activity还是ActivityThread发送handler消息启动的
所以还要代理拦截handler 的callback，还原要启动的intent

# 机制
## Binder 机制
https://github.com/francistao/LearningNotes/blob/master/Part1/Android/Binder机制.md

最底层的是Android的ashmen(Anonymous shared memory)机制，
<br />
它负责辅助实现内存的分配，以及跨进程所需要的内存共享。
<br />
AIDL(android interface definition language)对Binder的使用进行了封装，可以让开发者方便的进行方法的远程调用，后面会详细介绍。
<br />
Intent是最高一层的抽象，方便开发者进行常用的跨进程调用。
![enter description here][4]

    Stub--服务端
    Proxy--调用端（持有Stub的引用）
    
    得到引用：
    ServiceConnection 返回 BinderProxy，调用Stub.asInterface(BinderProxy)
    返回 Stub.Proxy
    
    调用方法:
    Stub.Proxy调用BinderProxy.transact(参数通过Parcel传，与Message有点相似),调用onTransact,调用具体方法
    
    提问：
    BinderProxy 是否就是Stub?
## Handler 机制
https://www.jianshu.com/p/9e4d1fab0f36 

![enter description here][5]

Handler持有Looper (绑定Thread)持有MessageQueue(相当于单链表Message的指针)
## 事件分发 机制
https://www.jianshu.com/p/e99b5e8bd67b
![enter description here][6]

    dispatchTouchEvent  ture,false,super（默认）
    onInterceptTouchEvent  false(默认),true
    onTouchEvent  false(默认),true
## 权限管理系统（底层的权限是如何进行 grant 的）


# 其他
## Android内存泄漏总结
https://juejin.im/entry/57a02f58c4c971005ae9022b

Memory Monitor
## Service
https://www.jianshu.com/p/4c798c91a613

    Service 运行在MainThread
    startService 不会停
    bindService 跟随绑定者
    IntentService = Service+HandlerThread(带Looper的Thread)
## 模块化与组件化
将一个程序按照其功能做拆分，分成相互独立的模块，以便于每个模块只包含与其功能相关的内容。模块我们相对熟悉,比如登录功能可以是一个模块,搜索功能可以是一个模块,汽车的发送机也可是一个模块。

模块化粒度更小,更侧重于重用,而组件化粒度稍大于模块,更侧重于业务解耦
其实形式上是一样的，只是思想上的区别

## 插件化（必然是组件化）
https://www.jianshu.com/p/b6d0586aab9f
<br />
https://github.com/Jerey-Jobs/AppPluginDemos 
<br />
http://blog.csdn.net/yulong0809/article/details/59113935 
<br />

Android应用程序的.Java文件在编译期会通过javac命令编译成.class文件，最后再把所有的.class文件编译成.dex文件放在.apk包里面。那么动态加载就是在运行时把插件apk直接加载到classloader里面的技术。而不是打包在apk中的

1. 通过DexClassLoader加载。
<br />
classloader
Android系统通过PathClassLoader来加载系统类和主dex中的类。 
<br />
而DexClassLoader则用于加载其他dex文件中的类。 
<br />
他们都是继承自BaseDexClassLoader，具体的加载方法是findClass。
2. 代理模式添加生命周期。
3. Hook思想跳过清单验证。
## 热修复，热更新(其实是一样的)
热修复其实是在插件化的基础上在进行替旧的Bug同名类
热更新方案发展至今，有很多团队开发过不同的解决方案，包括Dexposed、AndFix，(HotFix)Sophix，Qzone超级补丁的类Nuwa方式，微信的Tinker, 大众点评的nuwa、百度金融的rocooFix, 饿了么的amigo以及美团的robust、腾讯的Bugly热更新。 
## 增量更新
本质还是更新，但是只下载差分包,然后合并成新的apk,进行安装
需要服务器配合生成差分包,android 端进行组装.
<br />
开源算法有bsdiff (Binary diff)


## 自定义控件

# 模式

# 第三方
## Rxjava
https://www.jianshu.com/p/ec9849f2e510

 角色关系：
 
    Observable ( 被观察者 ) / Observer ( 观察者 )
    Flowable （被观察者）/ Subscriber （观察者）

操作符：

    转换类操作符(map flatMap concatMap flatMapIterable switchMap scan groupBy ...)；
    过滤类操作符(fileter take takeLast takeUntil distinct distinctUntilChanged skip skipLast ...)；
    组合类操作符(merge zip join combineLatest and/when/then switch startSwitch ...)。
## okhttp
## retrofit
## Dagger
## greenDao
用了插件生成代码
<br />
classpath "org.greenrobot:greendao-gradle-plugin:3.2.1"
## Glide

# 算法
## 集合(Collection)
![enter description here][7]
![enter description here][8]
![enter description here][9]
![enter description here][10]
## List
Arraylist 线程不安全
<br />
transient Object[] elementData;//数组存放地
1. DEFAULT_CAPACITY = 10;//只要加入数据，长度就变为10
2. int newCapacity = oldCapacity + (oldCapacity >> 1);//超出原来长度后的，变成算法,然后拷贝
<br />
if (newCapacity - minCapacity < 0)//如果加原来一半还不够，就用需要的长度
<br />
	newCapacity = minCapacity;
3. protected transient int modCount = 0;//防止操作错乱，也可以看出不支持多线程

Vector 线程安全
初始化时可设定扩容大小，默认是1倍


LinkedList 双向链表 线程不安全
<br />
Addlast 加到最后
<br />
RemoveFirst 去掉最前
<br />
Get 根据位置选择 从最后还从最前


## Map
HashMap线程不安全

    HashMapEntry<K, V>[] table;//存放链表的
    table[index] = new HashMapEntry<K, V>(key, value, hash, table[index]);//加入新数据
    int newCapacity = oldCapacity * 2;//扩容是直接2倍

HashTab 线程安全 

    public synchronized V put(K key, V value)  方法上加synchronized
ConcurrentHashMap 线程安全  方法内（即部分）加synchronized

    transient volatile Node<K,V>[] table;
    int c = (size >= (MAXIMUM_CAPACITY >>> 1)) ? MAXIMUM_CAPACITY ://扩容
            tableSizeFor(size + (size >>> 1) + 1);


LinkedHashMap (在hashmap基础上,将每一个entry加入双向环行链表)

    Header = new LinkedEntry<K,V>(key, value, hash, table[index], header, oldTail);
    private void makeTail(LinkedEntry<K, V> e) //实现lru算法的关键
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {false}

LRUCache原理就是用了LinkedHashMap 可以实现 图片三级缓存

    protected int sizeOf(K key, V value) {
        return 1;//可见他是按照个数来的，可以改成按照内存来
		
## Set
ArrayMap（SimpleArrayMap 没有继承map）

       int[] mHashes;//下标是key 和hash得到的，值是key的hash
        Object[] mArray;//放kay,value
扩容是线程安全的

ArraySet 和 ArrayMap 很像
## Queue
排序，
https://www.cnblogs.com/onepixel/articles/7674659.html
![enter description here][11]

    冒泡排序(白痴都懂)
    选择排序(闷牌完，全部拿起理顺时)
    插入排序(抓牌时就是这个)
    希尔排序(可看做分组进行插入排序)
    归并排序(可看做分段的选择排序，然后合并)
    快速排序
    堆排序
    桶排序
    计数排序
    基数排序



    B,B+树
    二叉树 
    深度遍历与广度遍历


有向无环图()

![enter description here][12]

# 示例
    动态代理,优缺点
    ams的原理
    wms原理
    为什么要设计binder这个玩意来用在Android的跨进程通信上面 
    什么叫aop,实现原理
    什么叫泛型擦除
    Android的权限是如何检查的
    什么是双亲委派?
        加载它的类加载器和这个类本身共同确立其在Java虚拟机中的唯一性
        加载任务委托给父类加载器，依次递归，如果父类加载器可以完成类加载任务，就成功返回；只有父类加载器无法完成此加载任务时，才自己去加载。
    什么叫threadlocal

IOC

    1. 运行时,注解+反射
    2. 编译时, 注解+（注解实现 AbstactProcessor子类）+ 代码生成 auto-commoon(AutoValue)
    Javapoet 



静默安装

    反射调用系统服务 packagemanage
    将app 制作成系统app，清单文件加一句，然后用系统工具签名
IM 

    Xmpp + openFire + smack	
Mirror是什么？

    取代 APT 的是更优的 JSR 269 API - "Pluggable Annotation Processing API"

Apt 是什么? aapt是什么?

    Apt -Annotation Processing Tool
    主要用于在编译期根据不同的注解类生成或者修改代码。APT运行于独立的JVM进程中（编译之前），并且在一次编译过程中可能会被多次调用
    
    AAPT - Android Asset Packaging Tool
    就可知道AAPT是Android资源打包工具





  [1]: ./images/1520836912803.jpg
  [2]: ./images/1520836947753.jpg
  [3]: ./images/1520836989441.jpg
  [4]: ./images/1520837023570.jpg
  [5]: ./images/1520837047941.jpg
  [6]: ./images/1520837142322.jpg
  [7]: ./images/1520837440464.jpg
  [8]: ./images/1520837449654.jpg
  [9]: ./images/1520837454311.jpg
  [10]: ./images/1520837461797.jpg
  [11]: ./images/1520837530988.jpg
  [12]: ./images/1520837549196.jpg